FROM liliancal/ubuntu-php-apache

COPY src/ /var/www/

RUN echo /var/www/index.html

EXPOSE 80

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]

